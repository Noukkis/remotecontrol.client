/**
 * @format
 * @flow
 */
import React, { Component } from 'react'
import SplashScreen from 'react-native-splash-screen';
import { createMaterialTopTabNavigator, createAppContainer } from 'react-navigation';
import { ThemeContext, getTheme } from 'react-native-material-ui';
import { Provider } from 'react-redux'
import MainView from './src/views/MainView';
import MenuView from './src/views/MenuView';
import Store from './src/redux/Store';
import Ctrl from './src/Ctrl';
import uiTheme from './src/uiTheme';

const { primaryColor, accentColor } = uiTheme.palette;

Ctrl.init(Store);

const routeConfig = {
  Main: MainView,
  Menu: MenuView
};

const navigatorConfig = {
  tabBarOptions: {
    style: {
      backgroundColor: primaryColor
    },
    indicatorStyle: {
      backgroundColor: accentColor
    },
    labelStyle: {
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: 'bold'
    }
  }
}

const Nav = createMaterialTopTabNavigator(routeConfig, navigatorConfig);
const AppContainer = createAppContainer(Nav);

export default class App extends Component {

  componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <Provider store={Store} >
        <ThemeContext.Provider value={getTheme(uiTheme)}>
          <AppContainer />
        </ThemeContext.Provider>
      </Provider>
    )
  }

};