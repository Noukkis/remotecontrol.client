import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Keyboard } from 'react-native';
import Button from './LayoutButton';

const KEYBOARD_LISTENER = 'keyboardDidHide';

export default class KeyboardButton extends Component {

    componentDidMount() {
        this.subscription = Keyboard.addListener(KEYBOARD_LISTENER, () => this.input.blur());
    }

    componentWillUnmount() {
        if(this.subscription) this.subscription.remove();
    }

    // Workaround because onKeyPress doesn't trigger on digitj
    onChangeText(text) {
        let last = text.slice(-1);
        if(/\d/.test(last)) this.props.onAction(actionFromKey({ nativeEvent: { key: last }}));
    }

    render() {
        const { width, onAction, actions, ...btnProps } = this.props;
        const style = { width: 100*width+'%' };
        return (
            <View style={[styles.view, style]}>
                <TextInput
                    style={styles.input}
                    ref={input => this.input = input}
                    onChangeText={text => this.onChangeText(text)}
                    onKeyPress={key => onAction(actionFromKey(key))}
                    onEndEditing={() => { this.input.clear(); onAction(actions[1]) }}
                />
                <Button width={1} {...btnProps} onAction={() => { this.input.focus(); onAction(actions[0]) }} />
            </View>
        )
    }
}

KeyboardButton.defaultProps = { onAction: () => {}, width: 1/2, action: [ [], [] ] };

const styles = StyleSheet.create({
    view: {
        flexWrap: 'wrap',
        flexDirection : 'row'
    },
    input: {
        display: 'none'
    }
});

function actionFromKey({ nativeEvent: { key } }) {
    return { type: 'keyboard', value: { key: key }};
}