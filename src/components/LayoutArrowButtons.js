import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import Button from './LayoutButton';

export default class LayoutArrowButtons extends Component {

    render() {
        const { height, onAction, actions } = this.props;
        return (
            <View style={styles.view}>
                <View style={styles.buttonUp}>
                    <Button width={1/3} icon='keyboard-arrow-up' onAction={() => onAction(actions[0])} />
                </View>
                <Button height={height} width={1/3} icon='keyboard-arrow-left' onAction={() => onAction(actions[1])} />
                <Button height={height} width={1/3} icon='keyboard-arrow-down' onAction={() => onAction(actions[2])} />
                <Button height={height} width={1/3} icon='keyboard-arrow-right' onAction={() => onAction(actions[3])} />
            </View>
        )
    }
}

LayoutArrowButtons.defaultProps = { height: 1, onAction: () => {}, action: [ [], [], [], [] ] };

const styles = StyleSheet.create({
    view: {
        flexWrap: 'wrap',
        flexDirection : 'row',
        padding: 10
    },
    buttonUp: {
        width: '100%',
        alignItems: 'center'
    }
});