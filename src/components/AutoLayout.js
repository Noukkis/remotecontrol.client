import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { withTheme } from 'react-native-material-ui';
import Button from './LayoutButton';
import LayoutArrowButtons from './LayoutArrowButtons';
import KeyboardButton from './KeyboardButton';
import Ctrl from '../Ctrl';

class AutoLayout extends Component {

    render() {
        let { components, theme } = this.props;
        let { backgroundColor } = theme.palette;
        let style = { backgroundColor };
        let layoutComponents = components.map((component, i) => (
            <LayoutComponent key={i+''} component={component} onAction={action => this.onAction(action)} />
        ));
        return (
            <View style={[styles.view, style]}>
                {layoutComponents}
            </View>
        )
    }

    onAction(action) {
        Ctrl.sendAction(action);
    }
}

function LayoutComponent({ component, onAction }) {
    switch(component.type) {
        case 'Button': return <Button onAction={action => onAction(action)} {...component} />
        case 'ArrowButtons': return <LayoutArrowButtons onAction={action => onAction(action)} {...component} />
        case 'KeyboardButton': return <KeyboardButton onAction={action => onAction(action)} {...component} />
        default: return null;
    }
}

const styles = StyleSheet.create({
    view: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%'
    }
});

export default withTheme(AutoLayout);