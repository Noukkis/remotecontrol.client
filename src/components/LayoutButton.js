import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Icon } from 'react-native-material-ui';

const HEIGHT_UNIT = 110;
const textSize = 42;

export default class LayoutButton extends Component {

    render() {
        const { icon, text, width, height, onAction, actions } = this.props;
        const style = { width: 100*width+'%', height: height*HEIGHT_UNIT };
        const iconComponent = icon ? <Icon name={icon} size={textSize} color='white' /> : null;
        return (
            <View style={[style, styles.view]}>
                <Button style={buttonStyle} raised primary icon={iconComponent} text={text} onPress={() => onAction(actions)} />
            </View>
        )
    }
}

LayoutButton.defaultProps = { text: '', width: 1/2, height: 1, onAction: () => {} };

const styles = StyleSheet.create({
    view: {
        padding: 10
    }
});

const buttonStyle = StyleSheet.create({
    container: {
        height: '100%'
    },
    text: {
        fontSize: textSize
    }
});