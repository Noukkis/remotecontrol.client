/**
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { connect } from 'react-redux'
import AutoLayout from '../components/AutoLayout';

class MenuView extends Component {

  render() {
    const { layouts, menu } = this.props;
    const components = (menu  >= 0) 
        ? layouts[menu].components
        : [];
    return (
      <AutoLayout components={components} />
    );
  }

}

function mapStateToProps({ layouts, menu }) {
  return { layouts, menu };
}

export default connect(mapStateToProps)(MenuView);