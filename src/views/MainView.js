/**
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import AutoLayout from '../components/AutoLayout';

class MenuView extends Component {

  render() {
    const { layouts, current } = this.props;
    const components = (current  >= 0) 
      ? layouts[current].components
      : [];
    return (
      <AutoLayout components={components} />
    );
  }

}

function mapStateToProps({ layouts, current }) {
  return { layouts, current };
}

export default connect(mapStateToProps)(MenuView);