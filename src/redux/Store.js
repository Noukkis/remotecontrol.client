import { createStore } from 'redux';
import updateLayout from './reducers/layoutReducer'

const defaultState = { layouts: [], menu: -1, current: -1 };

export default createStore(updateLayout, defaultState);