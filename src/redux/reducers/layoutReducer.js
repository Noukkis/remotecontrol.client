export const ActionType = {
  LayoutsList: 1,
  LayoutChange: 2,
  LayoutMenu: 3
};

export default function(state, action) {
  const { LayoutsList, LayoutChange, LayoutMenu } = ActionType;
  switch(action.type) {
    case LayoutsList: return layoutsListAction(state, action);
    case LayoutChange: return layoutChangeAction(state, action);
    case LayoutMenu: return layoutMenuAction(state, action);
    default: return state;
  }
}

function layoutsListAction(state, { layouts }) {
  return { ...state, layouts };
}

function layoutChangeAction(state, { layout }) {
  return { ...state, current: layout };
}

function layoutMenuAction(state, { layout }) {
  return { ...state, menu: layout };
}