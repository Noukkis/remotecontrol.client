import { COLOR } from 'react-native-material-ui';

const uiTheme = {
  palette: {
    primaryColor: COLOR.blueGrey400,
    backgroundColor: COLOR.blueGrey900,
    accentColor: COLOR.orange400
  },
};

export default uiTheme;