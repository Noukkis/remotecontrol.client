import SocketIOClient from 'socket.io-client';
import { Store } from 'redux';
import { ActionType } from '../redux/reducers/layoutReducer';

export default class Api {

  constructor(host, store) {
    this.store = store;
    const socket = SocketIOClient(host);
    socket.on('menu', layout => this._setMenuLayout(layout));
    socket.on('layouts', layouts => this._onLayoutsUpdate(layouts));
    socket.on('layout', layout => this._setCurrentLayout(layout));
    this.socket = socket;
  }

  _setMenuLayout(layout) {
    let type = ActionType.LayoutMenu;
    this.store.dispatch({ type, layout })
  }
  
  _onLayoutsUpdate(layouts) {
    let type = ActionType.LayoutsList;
    this.store.dispatch({ type, layouts })
  }
  
  _setCurrentLayout(layout) {
    let type = ActionType.LayoutChange;
    this.store.dispatch({ type, layout })
  }

  sendAction(action) {
    this.socket.emit('action', action);
  }

}