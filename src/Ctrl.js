import Api from './services/Api';

let api;
export default {
  
  init(store) {
    const host = 'http://192.168.1.136:3000';
    api = new Api(host, store);
  },

  sendAction(action){
    api.sendAction(action);
  }

}